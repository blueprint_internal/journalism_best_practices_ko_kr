﻿1
00:00:00,760 --> 00:00:02,873
Cette session vise à vous présenter

2
00:00:02,873 --> 00:00:04,987
l’outil Intelligence de CrowdTangle.

3
00:00:04,987 --> 00:00:06,929
Intelligence vous offre des statistiques

4
00:00:06,929 --> 00:00:08,871
accumulées (avec graphiques et

5
00:00:08,871 --> 00:00:10,814
diagrammes à l’appui) sur votre compte.

6
00:00:10,814 --> 00:00:12,994
Il fonctionne avec toutes les plates-formes

7
00:00:12,994 --> 00:00:15,174
prises en charge par CrowdTangle,

8
00:00:15,174 --> 00:00:19,565
notamment Facebook, Instagram, Twitter et Reddit.

9
00:00:19,565 --> 00:00:21,433
Cet outil permet aux éditeurs d’avoir

10
00:00:21,433 --> 00:00:23,301
une meilleure idée des tendances générales,

11
00:00:23,301 --> 00:00:26,548
et de mieux analyser les contenus qui sont efficaces

12
00:00:26,548 --> 00:00:28,737
et ceux qui ne le sont pas.

13
00:00:28,737 --> 00:00:31,191
Pour utiliser Intelligence, il vous suffit de saisir

14
00:00:31,191 --> 00:00:33,646
votre compte sur la barre de recherche supérieure.

15
00:00:34,901 --> 00:00:38,678
Vous pouvez trier les données par type de publication

16
00:00:38,678 --> 00:00:42,456
(photos, liens, vidéos) et par type d’interaction

17
00:00:42,456 --> 00:00:45,602
(mentions J’aime, commentaires et partages).

18
00:00:45,602 --> 00:00:48,716
Vous pouvez saisir jusqu’à cinq Pages différentes

19
00:00:48,716 --> 00:00:51,831
dans la barre de recherche supérieure

20
00:00:51,831 --> 00:00:54,956
afin d’effectuer une analyse concurrentielle.

21
00:00:54,956 --> 00:00:57,560
Plus important encore, vous pouvez

22
00:00:57,560 --> 00:01:00,165
exporter ce rapport en tant que PDF

23
00:01:00,165 --> 00:01:03,221
ou le télécharger sous forme de fichier CSV.

24
00:01:03,221 --> 00:01:05,539
Vous pouvez également convertir

25
00:01:05,539 --> 00:01:07,858
ce rapport en lien public,

26
00:01:07,858 --> 00:01:10,172
que vous pourrez ensuite partager avec les membres

27
00:01:10,172 --> 00:01:12,487
de votre équipe, qu’ils utilisent CrowdTangle ou non.

28
00:01:13,861 --> 00:01:16,031
Si vous avez des questions,

29
00:01:16,031 --> 00:01:18,000
n’hésitez pas à contacter l’équipe de CrowdTangle

30
00:01:18,000 --> 00:01:19,510
par l’intermédiaire du bouton

31
00:01:19,510 --> 00:01:21,219
de discussion instantanée en bas à droite.

32
00:01:21,219 --> 00:01:24,086
Amusez-vous bien avec CrowdTangle !

